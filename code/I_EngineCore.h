#ifndef I_ENGINECORE_H
#define I_ENGINECORE_H
#pragma once

#include "CommonDefines.h"
#include "InputHandler.h"

//-------------------------------------------------------------------------------------------
// Name: I_EngineCore
// Desc: Interface for enginecore
//------------------------------------------------------------------------------------------
class I_EngineCore
{
public:
	virtual ~I_EngineCore() {}

	virtual int InitEngineCore() = 0;
	virtual int RunEngineCore() = 0;
	

};

#endif //I_ENGINECORE_H