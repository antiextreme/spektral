#pragma once

#include <queue>

enum e_event_engine_type
{
	e_event_engine_type_none,
	e_event_engine_type_test,
	e_event_engine_type_mouse_pos

};

struct event_engine
{
	virtual ~event_engine() {}
	virtual e_event_engine_type get_type() { return e_event_engine_type_none; }
};

struct event_engine_test : public event_engine
{
	int m_test_int = 0;

	virtual e_event_engine_type get_type() override { return e_event_engine_type_test; }
};

struct event_engine_mouse_pos : public event_engine
{
	double m_x = 0;
	double m_y = 0;
	double m_x_prev = 0;
	double m_y_prev = 0;

	virtual e_event_engine_type get_type() override { return e_event_engine_type_mouse_pos; }
};


//---

struct EventEngineContainer
{
private:
	std::queue<event_engine*> m_events;
	//probably need mutexing across the engine

public:

	void add_event(event_engine* event)
	{
		m_events.push(event);
	}

	bool get_event(event_engine*& event_out) {

		if (m_events.empty() == false)
		{
			event_out = m_events.front();
			m_events.pop();
			return true;
		}
		return false;
	}

	void add_event_test(int in)
	{
		event_engine_test* p_event = new event_engine_test;
		
		p_event->m_test_int = in;
		
		add_event(p_event);
	}

	void add_event_mouse_pos(double x, double y, double x_prev, double y_prev)
	{
		event_engine_mouse_pos* p_event = new event_engine_mouse_pos;

		p_event->m_x = x;
		p_event->m_y = y;
		p_event->m_x_prev = x_prev;
		p_event->m_y_prev = y_prev;

		add_event(p_event);
	}

};