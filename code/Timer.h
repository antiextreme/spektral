#ifndef TIMER_H
#define TIMER_H

#pragma once

#include <chrono>

class Timer
{
public:

	Timer()
	{
		start = std::chrono::high_resolution_clock::now();
		stop = std::chrono::high_resolution_clock::now();
	}

	bool time_reached(double targettime)
	{
		// if that timne has reached or greater than the target time we specify, then return true
		if (get_milliseconds_elapsed() >= targettime)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	double get_milliseconds_elapsed()
	{
		if (isRunning)
		{
			auto elapsed = std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now() - start);
			return elapsed.count();
		}
		else
		{
			auto elapsed = std::chrono::duration<double, std::milli>(stop - start);
			return elapsed.count();
		}

	}

	void Restart()
	{
		isRunning = true;
		start = std::chrono::high_resolution_clock::now();
	}

	bool Stop()
	{
		if (!isRunning)
			return false;
		else
		{
			stop = std::chrono::high_resolution_clock::now();
			isRunning = false;
			return true;
		}	
	}

	bool Start()
	{
		if (isRunning)
		{
			return false;
		}
		else
		{
			start = std::chrono::high_resolution_clock::now();
			isRunning = true;
			return true;
		}
	}

private:
	bool isRunning = false;

#ifdef _WIN32
	std::chrono::time_point<std::chrono::steady_clock> start;
	std::chrono::time_point<std::chrono::steady_clock> stop;
#else
	std::chrono::time_point<std::chrono::steady_clock> start;
	std::chrono::time_point<std::chrono::steady_clock> stop;
#endif

};

#endif //TIMER_H

