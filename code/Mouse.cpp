#include "Mouse.h"
#include <iostream>

#define ROTATE_VELOCITY 0.001f

double Mouse::m_posX;
double Mouse::m_posY;
double Mouse::m_posX_prev;
double Mouse::m_posY_prev;

int Mouse::Init(EventEngineContainer* events_engine)
{
	I_Input::Init(events_engine);

	return 0;
}

void Mouse::mouseButtonCallback(GLFWwindow* m_window, int button, int action, int mods)
{
	//TODO Look at other way of doing this? // Event handling
	if (button == GLFW_MOUSE_BUTTON_1)
	{
		switch (action)
		{
		case GLFW_PRESS:
			std::cout << "Mouse button 1 Pressed" << std::endl;
			m_is_dragging = true;
			break;
		case GLFW_REPEAT:
			std::cout << "Mouse button 1 is Held" << std::endl;
			m_is_dragging = true;
			break;
		case GLFW_RELEASE:
			std::cout << "Mouse button 1 Release" << std::endl;
			m_is_dragging = false;
			break;
		default:
			break;
		}

	}
	if (button == GLFW_MOUSE_BUTTON_2)
	{
		switch (action)
		{
		case GLFW_PRESS:
			std::cout << "Mouse button 2 Pressed" << std::endl;
			break;
		case GLFW_REPEAT:
			std::cout << "Mouse button 2 is Held" << std::endl;
			break;
		case GLFW_RELEASE:
			std::cout << "Mouse button 2 Release" << std::endl;
			break;
		default:
			break;
		}
	}
	if (button == GLFW_MOUSE_BUTTON_3)
	{
		switch (action)
		{
		case GLFW_PRESS:
			std::cout << "Mouse button 3 Pressed" << std::endl;
			break;
		case GLFW_REPEAT:
			std::cout << "Mouse button 3 is Held" << std::endl;
			break;
		case GLFW_RELEASE:
			std::cout << "Mouse button 3 Release" << std::endl;
			break;
		default:
			break;
		}
	}
}

void Mouse::cursorPosCallback(GLFWwindow* m_window, double posX, double posY)
{
	Mouse::m_posX_prev = Mouse::m_posX;
	Mouse::m_posY_prev = Mouse::m_posY;

	glfwGetCursorPos(m_window, &posX, &posY);

	Mouse::m_posX = posX;
	Mouse::m_posY = posY;

	m_events_engine->add_event_mouse_pos(Mouse::m_posX, Mouse::m_posY, Mouse::m_posX_prev, Mouse::m_posY_prev);

}

void Mouse::scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
}
