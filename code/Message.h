#ifndef MESSAGE_H
#define MESSAGE_H

#pragma once

#include <string>

struct Message
{
	Message(std::string m) { m_message = m; }

	std::string m_message = "";

};

#endif //MESSAGE_H