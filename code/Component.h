#ifndef COMPONENT_H
#define COMPONENT_H
#pragma once

#include <string>

//-------------------------------------------------------------------------------------------
// Name: Component
// Desc: Component Interface that all components have to implement
//------------------------------------------------------------------------------------------
class Component
{
public:

	//Interface
	virtual void OnUpdate(double deltatime) = 0;
	virtual void OnMessage(const std::string message) = 0;

private:
	
};




#endif //COMPONENT_H
