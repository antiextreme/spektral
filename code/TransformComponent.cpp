#include "TransformComponent.h"

void TransformComponent::OnUpdate(double dt)
{
}

void TransformComponent::OnMessage(const std::string m)
{
	// e.g.	if (m == "rotateLeft")
	if (m == "rotateLeft")
	{
		yaw(rotateStepsize);
	}
	if (m == "rotateRight")
	{
		yaw(-rotateStepsize);
	}
	if (m == "rotateUp")
	{
		pitch(-rotateStepsize);
	}
	if (m == "rotateDown")
	{
		pitch(rotateStepsize);
	}
	if (m == "moveforward")
	{
		glm::vec3 v = glm::vec3(0, 0, -moveStepsize);
		std::cout << "Moving forward" << std::endl;
		translate(GetOrientation() * v);
	}
	if (m == "movebackwards")
	{
		glm::vec3 v = glm::vec3(0, 0, moveStepsize);
		std::cout << "Moving backwards" << std::endl;
		translate(GetOrientation() * v);

	}
	if (m == "moveleft")
	{
		glm::vec3 v = glm::vec3(-moveStepsize, 0, 0);
		std::cout << "Moving left" << std::endl;
		translate(GetOrientation() * v);
	}
	if (m == "moveright")
	{
		glm::vec3 v = glm::vec3(moveStepsize, 0, 0);
		std::cout << "Moving right" << std::endl;
		translate(GetOrientation() * v);

	}
	if (m == "reset")
	{
		m_position = glm::vec3(0, 0, 0);
		m_orientationYaw = glm::quat(1, 0, 0, 0);
		m_orientationPitch = glm::quat(1, 0, 0, 0);
	}
}
