#ifndef GL_CORE_H
#define GL_CORE_H
#pragma once

#include <glad/glad.h>
#include "CommonDefines.h"

#include "CameraComponent.h"
#include "GL_Shader.h"
#include "Model.h"

//Forward Declarations
class GL_Shader;


//-------------------------------------------------------------------------------------------
// Name: GL_Core
// Desc: The core class for most OpenGL related tasks
//------------------------------------------------------------------------------------------
class GL_Core
{
public:
	int  Init();
	void SetDefaultShaders();
	void UseShaderProgram();
	void ClearColourBuffer();

	void DrawModel(Model* model, const glm::mat4& modelMatrix);
	void SetCamera(const CameraComponent* camera);

	~GL_Core();

private:
	GL_Shader* m_defaultShader = nullptr; //The default shader
	GLuint m_defaultShaderProgram; //The default shader program

};


//ERROR CALLBACK FUNCTIONS
static void APIENTRY glDebugCallback(
	GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	std::string msgSource;
	switch (source) {
	case GL_DEBUG_SOURCE_API:
		msgSource = "WINDOW_SYSTEM";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		msgSource = "SHADER_COMPILER";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		msgSource = "THIRD_PARTY";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		msgSource = "APPLICATION";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		msgSource = "OTHER";
		break;
	}

	std::string msgType;
	switch (type) {
	case GL_DEBUG_TYPE_ERROR:
		msgType = "ERROR";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		msgType = "DEPRECATED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		msgType = "UNDEFINED_BEHAVIOR";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		msgType = "PORTABILITY";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		msgType = "PERFORMANCE";
		break;
	case GL_DEBUG_TYPE_OTHER:
		msgType = "OTHER";
		break;
	}

	std::string msgSeverity;
	switch (severity) {
	case GL_DEBUG_SEVERITY_LOW:
		msgSeverity = "LOW";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		msgSeverity = "MEDIUM";
		break;
	case GL_DEBUG_SEVERITY_HIGH:
		msgSeverity = "HIGH";
		break;
	}

	printf("glDebugMessage:\n%s \n type = %s source = %s severity = %s\n", message, msgType.c_str(), msgSource.c_str(), msgSeverity.c_str());
}

class GLErrorCallback
{
public:



	GLErrorCallback()
	{


#if _DEBUG
		// Enable the debug callback
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(glDebugCallback, nullptr);
		glDebugMessageControl(
			GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, true
		);
#endif
	}

private:

};


#endif GL_CORE_H