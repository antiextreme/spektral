#include "GL_Core.h"

int GL_Core::Init()
{
	//Call GL ERROR
	GLErrorCallback gl_errorCallback;

	
	
	return 0;
}

void GL_Core::SetDefaultShaders()
{
	m_defaultShader = new GL_Shader(m_defaultShaderProgram, "data/Shaders/defaultShader.vert", "data/Shaders/defaultShader.frag");
}

void GL_Core::UseShaderProgram()
{
	glUseProgram(m_defaultShaderProgram);
}


void GL_Core::ClearColourBuffer()
{
	//Clear the color buffers
	
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
}


void GL_Core::DrawModel(Model* model, const glm::mat4& modelMatrix)
{
	
	// set the model component of our shader to the object model
	glUniformMatrix4fv(glGetUniformLocation(m_defaultShaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(modelMatrix));

	model->Render(m_defaultShaderProgram);
	
}

void GL_Core::SetCamera(const CameraComponent* camera)
{
	// set the view and projection components of our shader to the camera values
	glm::mat4 projection = glm::perspective(glm::radians(camera->GetFOV()), 800.0f / 600.0f, 0.1f, 1000.0f);
	glUniformMatrix4fv(glGetUniformLocation(m_defaultShaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	glUniformMatrix4fv(glGetUniformLocation(m_defaultShaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(camera->GetViewMatrix()));

	// be sure to activate shader when setting uniforms/drawing objects
	glUniform3f(glGetUniformLocation(m_defaultShaderProgram, "objectColour"), 1.0f, 0.6f, 0.61f);
	glUniform3f(glGetUniformLocation(m_defaultShaderProgram, "lightColour"), 1.0f, 1.0f, 1.0f);
	glUniform3f(glGetUniformLocation(m_defaultShaderProgram, "lightPos"), 0.0f, 2.0f, -2.0f);
	glUniform3fv(glGetUniformLocation(m_defaultShaderProgram, "viewPos"), 1, glm::value_ptr(camera->GetPosition()));
}

GL_Core::~GL_Core()
{
	SAFE_DELETE(m_defaultShader);
}
