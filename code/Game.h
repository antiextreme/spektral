#ifndef GAME_H
#define GAME_H
#pragma once

#include "I_EngineCore.h"

#include "GL_Core.h"

class PlayerCharacter;
class ObserverHandler;
class GL_Core;

//-------------------------------------------------------------------------------------------
// Name: Game
// Desc: Responsible for handling the main game
//-------------------------------------------------------------------------------------------
class Game 
{

public:
	Game() {}
	Game(I_EngineCore* enginecore);

	CameraComponent* GetActiveCamera();

	int Init(ObserverHandler*& observerHandler);
	int Update();
	int Render(GL_Core*& p_gl_core);
	int DeInit();

	~Game();
	

	
private:
	
	//Engine core interface pointer
	I_EngineCore* m_engineinterfaceptr = nullptr;
	//Pointer to input handler
	InputHandler* m_Inputhandler	   = nullptr;

	std::vector<GameObject*> v_gameObjects;

	CameraComponent* m_active_camera = nullptr;
};


#endif //GAME_H