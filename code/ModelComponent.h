#ifndef MODELCOMPONENT_H
#define MODELCOMPONENT_H

#pragma once
#include "Component.h"

//Forward Declarations
class Model;


//-------------------------------------------------------------------------------------------
// Name: ModelComponent
// Desc: Component class that stores the data about a model and can return it
//------------------------------------------------------------------------------------------
class ModelComponent : public Component
{
public:
	ModelComponent(Model* model) : m_model(model) {};

	Model* getModel()
	{
		return m_model;
	}

	void OnUpdate(double dt) override{}
	void OnMessage(const std::string m) override{}

private:

	Model* m_model = nullptr;
};

#endif //MODELCOMPONENT_H