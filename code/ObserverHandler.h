#ifndef OBSERVERHANDLER_H
#define OBSERVERHANDLER_H
#pragma once


#include <string>
#include <iostream>
#include <vector>

class I_Observer;

//-------------------------------------------------------------------------------------------
// Name: Subject
// Desc: The subject class, manages observers
//------------------------------------------------------------------------------------------
class ObserverHandler
{
public:
	virtual ~ObserverHandler() = default;

	void AddObserver(I_Observer* obs);
	void RemoveObserver(I_Observer* obs);
	void SendNotifications(std::string message);
	void SomeEventHappened();

private:
	std::vector<I_Observer*> m_observers;
};





#endif //OBSERVERHANDLER_H