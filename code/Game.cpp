#include "Game.h"
#include "CommonDefines.h"
#include "Keyboard.h"
#include "CameraComponent.h"
#include "Mouse.h"
#include "EngineEvent.h"

#include "GameObjectFactory.h"

//Static global variables
std::vector<bool> Keyboard::m_key_buffer;

Game::Game(I_EngineCore* enginecore)
{
	m_engineinterfaceptr = enginecore;
}

int Game::Init(ObserverHandler*& observerHandler)
{
	// TEMP default camera for editor
	auto default_camera = GameObjectFactory::CreateGameobject(observerHandler, e_gameobject_type_entity, v_gameObjects);
	default_camera->AddComponent(new CameraComponent());
	m_Inputhandler = new InputHandler(default_camera);

	// TEMP test model
	auto test_model = GameObjectFactory::CreateGameobject(observerHandler, e_gameobject_type_entity, v_gameObjects);
	test_model->AddComponent(new TransformComponent());
	test_model->AddComponent(new ModelComponent(GameObjectFactory::GetModel("data/Models/nanosuit/nanosuit.obj")));

	// Set up camera pointer to a camera for rest of rendering code
	m_active_camera = v_gameObjects[0]->GetComponent<CameraComponent>();



	return 0;
}

int Game::Update()
{
	m_Inputhandler->handleInputs(Keyboard::m_key_buffer);

	m_active_camera->OnUpdate(69);

	return 0;
}

int Game::Render(GL_Core*& p_gl_core)
{
	p_gl_core->ClearColourBuffer();
	p_gl_core->UseShaderProgram();

	//Set camera to be the players camera
	p_gl_core->SetCamera(GetActiveCamera());

	//Iterate through v_entities and draw its models
	for (auto gameObject : v_gameObjects)
	{
		if (gameObject->GetComponent<ModelComponent>()) {
			Model* model = gameObject->GetComponent<ModelComponent>()->getModel();
			glm::mat4 matrix = gameObject->GetComponent<TransformComponent>()->getModelMatrix();
			p_gl_core->DrawModel(model, matrix);
		}
	}

	return 0;
}

int Game::DeInit()
{
	return 0;
}

CameraComponent* Game::GetActiveCamera()
{
	return m_active_camera;
}

Game::~Game()
{
	
	for (auto gameObject : v_gameObjects)
	{
		SAFE_DELETE(gameObject);
		v_gameObjects.clear();
	}
	SAFE_DELETE(m_active_camera);
	SAFE_DELETE(m_Inputhandler);
	
}
