#ifndef MESSAGEBUS_H
#define MESSAGEBUS_H

#pragma once

#include <queue>
#include "Message.h"
#include <iostream>

class MessageBus
{
	std::queue<Message> m_messageQueue;

	int add_message(Message m) { m_messageQueue.push(m); }

	int process_next();

};

int MessageBus::process_next()
{
	while (!m_messageQueue.empty())
	{
		std::cout << "msg: " << m_messageQueue.front().m_message << std::endl;

		m_messageQueue.pop();
	}

	return 0;
}

#endif //MESSAGEBUS_H
