#ifndef I_OBSERVER_H
#define I_OBSERVER_H
#pragma once

#include <string>




//-------------------------------------------------------------------------------------------
// Name: I_Observer
// Desc: The observer/listener interface
//------------------------------------------------------------------------------------------
class I_Observer
{
public:
	virtual void onEvent(std::string msg) = 0;

private:

};





#endif //I_OBSERVER_H