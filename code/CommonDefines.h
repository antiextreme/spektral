
// SAFE DELETE MACROS

#define SAFE_DELETE(a) if( (a) != NULL ) delete (a); (a) = NULL;

#define SAFE_DELETE_ARRAY(a) if( (a) != NULL ) delete (a[]); (a) = NULL;

#define DEBUG_MODE 1
//

