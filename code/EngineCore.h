#ifndef ENGINECORE_H
#define ENGINECORE_H

#pragma once

#include "I_EngineCore.h"
#include "GL_Core.h"
#include "GL_Window.h"

class Game;

class ObserverHandler;
struct EventEngineContainer;

struct event_engine;

class Mouse;
class Keyboard;

//-------------------------------------------------------------------------------------------
// Name: EngineCore implements I_EngineCore
// Desc: Implements I_EngineCore interface responsible for running the engine core 
//-------------------------------------------------------------------------------------------
class EngineCore : public I_EngineCore
{
public:
	EngineCore() {};

	int InitEngineCore() override;
	int RunEngineCore()  override;

	void ProcessEventEngine(EventEngineContainer* events_engine, event_engine* p_event);

	~EngineCore() override;

private:

	I_EngineCore* m_p_engineinterfaceptr = nullptr;

	EventEngineContainer* m_p_events_engine = nullptr;
	ObserverHandler* m_p_observer_handler = nullptr;
	Game* m_p_game = nullptr;

	GL_Core* m_p_gl_core = nullptr;
	GL_Window* m_p_gl_window = nullptr;

	Mouse* m_p_mouse = nullptr;
	Keyboard* m_p_keyboard = nullptr;

	GLFWwindow* m_p_window = nullptr;

};

#endif //ENGINECORE_H