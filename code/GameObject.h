#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#pragma once

#include <unordered_map>
#include <typeindex>
#include <Component.h>

#include "I_Observer.h"

#include "TransformComponent.h"
#include "CameraComponent.h"
#include "ModelComponent.h"


//-------------------------------------------------------------------------------------------
// Name: GameObject
// Desc: Any object in the game, can store components and listen for events
//------------------------------------------------------------------------------------------
class GameObject : public I_Observer
{
public:



	template<typename T>
	T* GetComponent()
	{
		auto iter = m_components.find(typeid(T));

		if (iter != std::end(m_components))
		{
			// if found dynamic cast the component pointer and return it
			return dynamic_cast<T*>(iter->second);
		}

		// return null if we don't have a component of that type
		return nullptr;	
	}

	template <typename T>
	void AddComponent(T* comp)
	{
		// add the component to unoreder map with hash of its typeid
		m_components[typeid(T)] = comp;
	}

	virtual void OnUpdate(double dt) = 0;
	virtual void OnMessage(const std::string message) = 0;

	virtual void OnNotify(std::string msg) {};


private:
	std::unordered_map<std::type_index, Component*> m_components;
};



#endif //GameObject_H