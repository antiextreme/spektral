#ifndef I_INPUT_H
#define I_INPUT_H

#pragma once

#include "EngineEvent.h"

//-------------------------------------------------------------------------------------------
// Name: I_Input
// Desc: Interface for peripherals such as mouse and keyboard
//------------------------------------------------------------------------------------------


class I_Input {

public:
	static EventEngineContainer* m_events_engine;

	virtual int Init(EventEngineContainer* events_engine);
	virtual int Update() = 0;

};

#endif //I_INPUT_H