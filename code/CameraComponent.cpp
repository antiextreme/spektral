#include "CameraComponent.h"

#include "Mouse.h"

bool Mouse::m_is_dragging;

void CameraComponent::OnUpdate(double dt)
{

	if (Mouse::m_is_dragging)
	{
		cameraQuatRotate((float)m_deltayPos * m_rotateSpeed, (float)m_deltaxPos * m_rotateSpeed);

		//position += orientation * relativePos;

		SetOrientation(GetOrientation());

	}

	m_deltaxPos = 0;
	m_deltayPos = 0;

}
