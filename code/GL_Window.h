#ifndef GL_WINDOW_H
#define GL_WINDOW_H
#pragma once

#include "glad/glad.h"
#include "glfw3.h"
#include "glfw3native.h"

#include "Graphics_enums.h"
#include "CommonDefines.h"

#define WINDOW_WIDTH_DEFAULT 800
#define WINDOW_HEIGHT_DEFAULT 450


//-------------------------------------------------------------------------------------------
// Name: GL_Window
// Desc: The main GLFW Window
//------------------------------------------------------------------------------------------
class GL_Window
{
public:
	//Default Constructor
	GL_Window();
	~GL_Window();

	int Init();
	int Update();
	int DeInit();

	//Get Window Sizes
	int GetWindowWidth() { return this->m_window_width; }
	int GetWindowHeight() { return this->m_window_height; }

	//Set Window Sizes
	void SetWindowWidth(int width) { this->m_window_width = width; }
	void SetWindowHeight(int height) { this->m_window_height = height; }


	//Toggle VSYNC
	int SetVsync(int toggle) { m_vsync = toggle; }

	//Get the GLFW Window
	GLFWwindow* GetWindow();
	//Set Window modes
	GLFWmonitor* SetWindowMode(e_windowMode mode);
private:
	//Main Window
	GLFWwindow* m_window = nullptr;


	//Window Modes
	int m_vsync = 0;
	int m_windowMode = e_windowMode_windowed;

	//Window size
	int m_window_width = 800;
	int m_window_height = 600;

};



#endif //GL_WINDOW_H