#include "EngineCore.h"

#include "GL_Core.h" //GL_Core first always (for now), includes glad
#include "GL_Window.h"

#include "Game.h"

#include "ObserverHandler.h"
#include "GameObjectFactory.h"
#include "EngineEvent.h"

#include "Keyboard.h"
#include "Mouse.h"

#include "CameraComponent.h"



int EngineCore::InitEngineCore()
{
	//Instantiations
	m_p_gl_window	= new GL_Window();
	m_p_gl_core		= new GL_Core();
	m_p_keyboard  = new Keyboard();
	m_p_mouse		= new Mouse();
	m_p_game		= new Game(this);

	
	m_p_events_engine = new EventEngineContainer();
	m_p_observer_handler = new ObserverHandler();
	
	//Inititalizations
	this->m_p_gl_window->Init(); 	//Initialize GL_Window
	this->m_p_gl_core->Init();		//Initialize GL_Core
	this->m_p_keyboard->Init(m_p_events_engine);   //Initialize Keyboard
	this->m_p_mouse->Init(m_p_events_engine);      // Initialize Mouse

	this->m_p_game->Init(m_p_observer_handler);

	//Initialize shaders and model
	m_p_gl_core->SetDefaultShaders();


	// ------------------ GLFW Callback Functions ------------------------ //
	glfwSetKeyCallback(m_p_gl_window->GetWindow(), m_p_keyboard->keyCallback);
	glfwSetMouseButtonCallback(m_p_gl_window->GetWindow(), m_p_mouse->mouseButtonCallback);
	glfwSetCursorPosCallback(m_p_gl_window->GetWindow(), m_p_mouse->cursorPosCallback);


	return 0;
}

int EngineCore::RunEngineCore()
{
	//set delta time
	double lastTime = glfwGetTime();
	static int FPSCounter = 0;
	static std::string FPSstring = "FPS: 0";

	//Main game loop
	while (1)
	{
		//------------------FPS COUNTER -----------------------------//
		double currentTime = glfwGetTime();
		FPSCounter++;
		if (currentTime - lastTime >= 1.0)
		{
			printf("%f FPS\n", double(FPSCounter));
			printf("%f ms/frame\n", 1000 / double(FPSCounter));
			FPSCounter = 0;
			lastTime += 1.0f;
		}

		//If we wanna close the window
		if (glfwWindowShouldClose(m_p_gl_window->GetWindow()))
		{
			m_p_gl_window->DeInit();
			return -1;
		}

		
		//------------------OPENGL DRAW ---------------------------//
		
		m_p_game->Render(m_p_gl_core);
	
		//------------------OPENGL DRAW END---------------------------//



		//----------------- UPDATE FUNCTIONS--------------------------//
		//Update main window render loop
		this->m_p_gl_window->Update();

		event_engine* p_event = new event_engine;
		while (m_p_events_engine->get_event(p_event))
		{
			ProcessEventEngine(m_p_events_engine, p_event);
		}

		//Update main game 
		m_p_game->Update();
	}

	return 0;
}

void EngineCore::ProcessEventEngine(EventEngineContainer* events_engine, event_engine* p_event)
{
	switch (p_event->get_type())
	{
	case e_event_engine_type_test:
	{
		event_engine_test* p_event_type = static_cast<event_engine_test*>(p_event);

		//printf("Magic number: %i \n", int(p_event_type->m_test_int));
	} break;
	case e_event_engine_type_mouse_pos:
	{
		event_engine_mouse_pos* p_event_type = static_cast<event_engine_mouse_pos*>(p_event);

		m_p_game->GetActiveCamera()->ProcessMouseInputPos(p_event_type->m_x, p_event_type->m_y, p_event_type->m_x_prev, p_event_type->m_y_prev);
		//printf("Mouse Pos: %d %d \n", p_event_type->m_x, p_event_type->m_y);
	} break;

	}
}

EngineCore::~EngineCore()
{
	SAFE_DELETE(m_p_gl_window);
	SAFE_DELETE(m_p_gl_core);
	SAFE_DELETE(m_p_keyboard);
	SAFE_DELETE(m_p_mouse);
	SAFE_DELETE(m_p_game);
	SAFE_DELETE(m_p_events_engine);
	SAFE_DELETE(m_p_observer_handler);
}
