#ifndef MOUSE_H
#define MOUSE_H
#pragma once

#include "I_Input.h"
#include <glfw3.h>
#include <glfw3native.h>


//-------------------------------------------------------------------------------------------
// Name: Mouse
// Desc: For handling mouse input
//------------------------------------------------------------------------------------------
class Mouse : I_Input
{
public:

	static bool m_is_dragging;

	static double m_posX;
	static double m_posY;
	static double m_posX_prev;
	static double m_posY_prev;

	static void mouseButtonCallback(GLFWwindow* m_window, int button, int action, int mods);
	static void cursorPosCallback(GLFWwindow* m_window, double posX, double posY);
	static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);

	int Init(EventEngineContainer* events_engine) override;
	int Update() override { return 0; }


};




#endif //MOUSE_H