#include "GL_Shader.h"


GL_Shader::GL_Shader(GLuint& shaderprogram, std::string vert, std::string frag)
{
	try
	{
		// Load contents of vertex file
		std::ifstream inFile(vert);
		if (!inFile) {
			std::string errorMsg = "Error opening shader file: " + vert + "\n";
			fprintf(stderr, errorMsg.c_str());
			exit(1);
		}

		std::stringstream code;
		code << inFile.rdbuf();
		inFile.close();
		std::string codeStr(code.str());
		const GLchar* vertex_shader[] = { codeStr.c_str() };

		// Load contents of fragment file
		std::ifstream inFile2(frag);
		if (!inFile2) {
			std::string errorMsg = "Error opening shader file: " + frag + "\n";
			fprintf(stderr, errorMsg.c_str());
			exit(1);
		}

		std::stringstream code2;
		code2 << inFile2.rdbuf();
		inFile2.close();
		std::string codeStr2(code2.str());
		const GLchar* fragment_shader[] = { codeStr2.c_str() };

		// vertex shader
		int vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, 1, vertex_shader, NULL);
		glCompileShader(vertexShader);
		// check for shader compile errors
		int success;
		char infoLog[512];
		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		}
		// fragment shader
		int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, 1, fragment_shader, NULL);
		glCompileShader(fragmentShader);
		// check for shader compile errors
		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
		}
		// link shaders
		shaderprogram = glCreateProgram();
		glAttachShader(shaderprogram, vertexShader);
		glAttachShader(shaderprogram, fragmentShader);
		glLinkProgram(shaderprogram);
		// check for linking errors
		glGetProgramiv(shaderprogram, GL_LINK_STATUS, &success);
		if (!success) {
			glGetProgramInfoLog(shaderprogram, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
		}
			//Cleanup
			glDeleteShader(vertexShader);
			glDeleteShader(fragmentShader);
		}

	catch (std::ifstream::failure e)
	{
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
	}

	
}
