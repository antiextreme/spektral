#ifndef GLSHADER_H
#define GLSHADER_H

#pragma once

#include <glad/glad.h>

#include <iostream>

#include <string>
#include <fstream>
#include <sstream>

//-------------------------------------------------------------------------------------------
// Name: GL_Shader
// Desc: The core class for creation of shaders
//------------------------------------------------------------------------------------------
class GL_Shader
{
public:

	GL_Shader(GLuint& shaderprogram, std::string vert, std::string frag);

private:

	std::string m_vertex_shader_filePath;
	std::string m_fragment_shader_filePath;
};



#endif //GLSHADER_H