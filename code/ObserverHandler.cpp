#include "ObserverHandler.h"
#include "I_Observer.h"
#include <algorithm>

void ObserverHandler::AddObserver(I_Observer* obs)
{
	m_observers.push_back(obs);
}

void ObserverHandler::RemoveObserver(I_Observer* obs)
{
	m_observers.erase(std::remove(m_observers.begin(), m_observers.end(), obs), m_observers.end());
}

void ObserverHandler::SendNotifications(std::string message)
{
	for (auto* observer : m_observers)
	{
		observer->OnNotify(message);
	}
}

void ObserverHandler::SomeEventHappened()
{
	SendNotifications("Isac knobby");
}
