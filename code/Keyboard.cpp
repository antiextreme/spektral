#include "Keyboard.h"

int Keyboard::Init(EventEngineContainer* events_engine)
{
	I_Input::Init(events_engine);

	// make space for the keybuffer
	m_key_buffer.resize(m_key_bufferSize);
	std::fill(m_key_buffer.begin(), m_key_buffer.end(), false);

	return 0;
}

int Keyboard::Update()
{
	return 0;
}

void Keyboard::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{

	if (key == GLFW_KEY_UNKNOWN || key > m_key_bufferSize)
	{
		return;
	}
	m_key_buffer[key] = ((action == GLFW_PRESS || action == GLFW_REPEAT));

	// Close window
	if (glfwGetKey(window, GLFW_KEY_ESCAPE == GLFW_PRESS))
	{
		glfwSetWindowShouldClose(window, true);
	}
}
