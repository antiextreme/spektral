#include "GL_Window.h"
#include "GUIManager.h"

#include <iostream>

GUIManager& g_GUIManager = GUIManager::getInstance();

GL_Window::GL_Window()
{
}

//Function Implementations
int GL_Window::Init()
{

	// initialise a window and let GLFW know that it should target opengl version 4.6
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//Create the window
	this->m_window = glfwCreateWindow(m_window_width, m_window_height, "Spektral Engine", SetWindowMode(e_windowMode_windowed), nullptr);
	if (this->m_window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	// make this new window our current context, THEN try to initialise GLAD function ptrs
	glfwMakeContextCurrent(this->m_window);

	//setup glad loader
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialise GLAD" << std::endl;
		return -1;
	}


#if DEBUG_MODE	
	g_GUIManager.InitImGui(m_window);
#endif


	return 0;
}

int GL_Window::Update()
{
	assert(m_window != nullptr);

	//VSYNC SWAP CHAIN
	if (m_vsync) { glfwSwapInterval(1); }
	else { glfwSwapInterval(0); }


#if DEBUG_MODE
	if (m_window != nullptr)  //window safeguard
	{
		g_GUIManager.RenderImGui();
	}
#endif

	if (m_window != nullptr)  //window safeguard
	{
		//Poll and call swap chain
		glfwPollEvents();
		glfwSwapBuffers(this->m_window);
	}

	return 0;
}

GLFWmonitor* GL_Window::SetWindowMode(e_windowMode mode)
{
	this->m_windowMode = mode;

	if (this->m_windowMode == e_windowMode_borderless_windowed) { return nullptr; } // todo work out bordereless window 
	else if (this->m_windowMode == e_windowMode_exclusive_fullscreen) { return glfwGetPrimaryMonitor(); }
	else { return nullptr; } // normal windowed
}

GLFWwindow* GL_Window::GetWindow()
{
	if (m_window != nullptr)  //window safeguard
	{
		return this->m_window;
	}
	else
	{
		assert(m_window);
		return false;
	}
}


//End of window
int GL_Window::DeInit()
{

	glfwTerminate();

#if DEBUG_MODE
	g_GUIManager.DeinitImGui(); //Need to destroy imgui after glfw terminates
#endif

	return 0;
}
//Destructor call
GL_Window::~GL_Window()
{

	//Clean up

}
