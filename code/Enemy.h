#ifndef ENTITYFACTORY
#define ENTITYFACTORY


#pragma once

#include <unordered_map>
#include <typeindex>
#include <iostream>

#include "Entity.h"
#include "Component.h"

//Step 1, Add enemies to enemy type enum
enum EntityType
{
	ET_Goblin, 
	ET_EvilWizard, 
	ET_Alien,

};

//Step 2, Add enemies

//Goblin
class EnemyGoblin : public Entity
{
public:

	void on_update(float deltatime) override { }

	void on_message(const std::string message) override
	{
		if (message == "keyPress")
		{
			std::cout << "I am a Goblin!" << std::endl;
		}
	}

private:

};

//Evil Wizard
class EnemyEvilWizard : public Entity
{
public:
	void on_update(float deltatime) override { }

	void on_message(const std::string message) override
	{
		if (message == "keyPress")
		{
			std::cout << "I am an evil Wizard!" << std::endl;
		}
	}

private:

};

//Alien
class EnemyAlien : public Entity
{
public:

	void on_update(float deltatime) override { }

	void on_message(const std::string message) override
	{
		if (message == "keyPress")
		{
			std::cout << "I am an Alien!" << std::endl;
		}
	}

private:

};


class EnemyFactory
{
public:

	static Entity* create_enemy(EntityType et_type) 
	{
		//Depending on enemy type we instantiate and return the enemies as completely constructed objects
		if (et_type == ET_Goblin)
		{
			return new EnemyGoblin();
		}
		else if (et_type == ET_EvilWizard)
		{
			return new EnemyEvilWizard();
		}
		else if (et_type == ET_Alien)
		{
			return new EnemyAlien();
		}
		else //if nothing
		{ 
			assert(false); // not supposed to be passing nothing
			return false;
		}
		
	}
	
private:

};


#endif //ENTITYFACTORY