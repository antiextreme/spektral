#ifndef GAMEOBJECTFACTORY_H
#define GAMEOBJECTFACTORY_H
#pragma once

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>

#include "Model.h"
#include "Entity.h"

class ObserverHandler;

//Global vars
static glm::vec3 defaultPos = glm::vec3(0.0f, 0.0f, 0.0f);
static glm::quat defaultOri = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);


//All GameObject types specified for factory creation
enum e_gameobject_type
{
	e_gameobject_type_gameobject,
	e_gameobject_type_entity
	
};


//-------------------------------------------------------------------------------------------
// Name: GameObjectFactory
// Desc: The main GameObject factory, responsible for the creation of entities
//------------------------------------------------------------------------------------------
class GameObjectFactory
{
public:

	static GameObject* CreateGameobject(ObserverHandler* obshandler, e_gameobject_type et_type, std::vector<GameObject*>& storage_dest);  //Returns the creation of the object

	static Model*  GetModel(std::string filename);  //Creates new model and stores in map, if it doesn't already exist, otherwise simply returns it

	
	
private:
	static std::unordered_map<std::string, Model*> um_models;

	
};



#endif //GAMEOBJECTFACTORY_H





