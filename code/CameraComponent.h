#ifndef CAMERACOMPONENT_H
#define CAMERACOMPONENT_H
#pragma once


#include "Component.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>



//-------------------------------------------------------------------------------------------
// Name: CameraComponent
// Desc: The main camera
//-------------------------------------------------------------------------------------------
class CameraComponent : public Component
{
private:
	float m_rotateSpeed = 0.005f;
	double m_deltaxPos = 0;
	double m_deltayPos = 0;
	glm::vec3 m_position;
	glm::quat m_orientation;
	float m_fov;

public:
	CameraComponent() : m_position(0), m_orientation(1, 0, 0, 0), m_fov(45) { }
	CameraComponent(const glm::vec3& pos) : m_position(pos), m_orientation(1, 0, 0, 0), m_fov(45) { }
	CameraComponent(const glm::vec3& pos, const glm::quat& orient) : m_position(pos), m_orientation(orient), m_fov(45) { }

	void LookAt(const glm::vec3& target) { m_orientation = (glm::toQuat(glm::lookAt(m_position, target, glm::vec3(0, 1, 0)))); }

	glm::quat quaternion(glm::vec3 axis, float angle)
	{
		glm::quat l_rotation;

		l_rotation.w = cos(angle / 2);
		l_rotation.x = sin(angle / 2) * axis.x;
		l_rotation.y = sin(angle / 2) * axis.y;
		l_rotation.z = 0; //! constraining z to remove unwanted roll

		return l_rotation;
	}
	void cameraQuatRotate(float pitch, float yaw)
	{
		m_orientation = (glm::normalize(quaternion(glm::vec3(0.0f, 1.0f, 0.0f), yaw) * m_orientation));
		m_orientation = (m_orientation * (glm::normalize(quaternion(glm::vec3(1.0f, 0.0f, 0.0f), pitch))));
	}

	const glm::vec3& GetPosition() const { return m_position; }
	const glm::quat& GetOrientation() const { return m_orientation; }
	const float& GetFOV() const { return m_fov; }

	void SetPosition(const glm::vec3 newPosition) { m_position = newPosition; }
	void SetOrientation(const glm::quat newOrientation) { m_orientation = newOrientation; }

	glm::mat4 GetViewMatrix() const { return glm::translate(glm::mat4_cast(inverse(m_orientation)), -m_position); }

	void SetFOV(float fov) { m_fov = fov; }

	void OnUpdate(double dt) override;

	void OnMessage(const std::string message) override {}

	void ProcessMouseInputPos(double xPos, double yPos, double xPos_prev, double yPos_prev) {

		m_deltaxPos = (xPos_prev - xPos); //! Delta x, change in mouseposX
		m_deltayPos = (yPos_prev - yPos); //! Delt y, change in mouseposY


	}

	void ProcessMouseInputButton() {

		/*if (glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_LEFT))
		{

		}*/

	}

};


#endif //CAMERACOMPONENT_H