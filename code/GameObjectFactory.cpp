#include "GameObjectFactory.h"

#include "Component.h"
#include "TransformComponent.h"
#include "ModelComponent.h"
#include "ObserverHandler.h"


//Static variables
std::unordered_map<std::string, Model*> GameObjectFactory::um_models;

//-------------------------------------------------------------------------------------------
// Name: NanoSuitTestObject
// Desc: A test object for the factory
//-------------------------------------------------------------------------------------------
class NanoSuitTestObject : public Entity //Step 1. Define GameObject here
{
public:
	NanoSuitTestObject()
	{
		
		TransformComponent* tc = new TransformComponent(defaultPos, defaultOri);
		ModelComponent* md = new ModelComponent(GameObjectFactory::GetModel("data/Models/nanosuit/nanosuit.obj"));

		AddComponent(tc);
		AddComponent(md);
	}

	void OnUpdate(double dt) override { }
	void OnMessage(const std::string message) override { }
	virtual void OnNotify(std::string message) 
	{
		std::cout << "Recieved Notification: " << message << std::endl;
		
	}


};

enum class CameraViewState
{
	firstPersonCamera,
	thirdPersonCamera
};


//-------------------------------------------------------------------------------------------
// Name: PlayerCharacter
// Desc: The main player character
//-------------------------------------------------------------------------------------------
class PlayerCharacter : public GameObject
{
public:

	// constructor
	PlayerCharacter(glm::vec3 position, glm::quat orientation)
	{
		TransformComponent* tc = new TransformComponent(position, orientation);
		CameraComponent* cc = new CameraComponent(position, orientation);
		AddComponent(tc);
		AddComponent(cc);
		SetCameraPositionFromTransformComponent(tc);
	}

	void OnUpdate(double dt) override
	{

	}

	void OnMessage(const std::string msg) override
	{

		TransformComponent* tc = GetComponent<TransformComponent>();

		if (msg == "setCameraFirstPerson")
		{
			m_cameraState = CameraViewState::firstPersonCamera;
			std::cout << "Changed to first person camera:" << std::endl;
		}
		else if (msg == "setCameraThirdPerson")
		{
			m_cameraState = CameraViewState::thirdPersonCamera;
			std::cout << "Changed to third person camera:" << std::endl;
		}
		else
		{
			// pass message to transform component
			tc->OnMessage(msg);
		}
		SetCameraPositionFromTransformComponent(tc);
	}

	void SetCameraPositionFromTransformComponent(TransformComponent* tc)
	{
		// get resulting position
		glm::vec3 pos = tc->GetPosition();
		glm::quat orient = tc->GetOrientation();
		// camera behind and above us OR at centre...

		glm::vec3 relativePosition;		// could use ? operator here
		if (m_cameraState == CameraViewState::thirdPersonCamera)
		{
			relativePosition = glm::vec3(0, 0, 0);		//above and behind
		}
		else
		{
			relativePosition = glm::vec3(0, 0, 5);	//just in front of model
		}
		pos += orient * relativePosition;

		// put camera there
		CameraComponent* cc = GetComponent<CameraComponent>();
		cc->SetPosition(pos);
		cc->SetOrientation(orient);
	}

	glm::vec3 GetEulerAngles()
	{
		TransformComponent* tc = GetComponent<TransformComponent>();

		return tc->GetEulerAngles();
	}


private:
	CameraViewState m_cameraState{ CameraViewState::thirdPersonCamera };
};


GameObject* GameObjectFactory::CreateGameobject(ObserverHandler* obshandler, e_gameobject_type et_type, std::vector<GameObject*>& storage_dest) //Step 2. Return it from the list of enums so user can grab it
{
	//Depending on type we instantiate and return 
	if (et_type == e_gameobject_type_entity)
	{
		Entity* tempEntity = new Entity();
		obshandler->AddObserver(tempEntity); // if you dont want to observe then unsubscribe

		storage_dest.push_back(tempEntity);

		return tempEntity;
	}
	//ADD MORE RETURNS HERE else if (et_type == ET_someobject)
	else //if nothing
	{		
		return false;
	}
}

//Model return func
Model* GameObjectFactory::GetModel(std::string filename)
{
	auto iter = GameObjectFactory::um_models.find(filename);
	if (iter != std::end(GameObjectFactory::um_models))
	{
		// if found return the pointer
		return iter->second;
	}
	else
	{
		// not found so load model
		Model* thisModel = new Model(filename);
		GameObjectFactory::um_models[filename] = thisModel;

		return thisModel;
	}
}


