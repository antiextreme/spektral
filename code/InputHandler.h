#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H
#pragma once

#include <map>
#include <vector>

#include "GameObject.h"


//-------------------------------------------------------------------------------------------
// Name: InputCommand
// Desc: The Command pattern for execution of messages via inputhandler
//------------------------------------------------------------------------------------------
class InputCommand
{
public:
	virtual void execute(GameObject& GameObject) = 0;
	virtual ~InputCommand() {}

private:
};


class MoveForwardCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("moveforward");
	}
};

class MoveBackwardsCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("movebackwards");
	}
};

class MoveLeftCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("moveleft");
	}
};

class MoveRightCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("moveright");
	}
};

class RotateLeftCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("rotateLeft");
	}
};

class RotateRightCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("rotateRight");
	}
};

class RotateUpCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("rotateUp");
	}
};

class RotateDownCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("rotateDown");
	}
};

class SetFirstPersonCameraCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("setCameraFirstPerson");
	}	
};

class SetThirdPersonCameraCommand : public InputCommand
{
	void execute(GameObject& GameObject)
	{
		GameObject.OnMessage("setCameraThirdPerson");
	}	
};

//-------------------------------------------------------------------------------------------
// Name: InputHandler
// Desc: Handle input events and send messages to entities
//------------------------------------------------------------------------------------------
class InputHandler
{
private:

	GameObject* m_object;

	std::map<int, InputCommand*> m_controlMapping;

public:

	InputHandler(GameObject* playerCube) : m_object(playerCube)
	{
	
		// the idea will be to map the keys directly from a config file that can be loaded in
		// and changed on the fly
		m_controlMapping[(int)'W'] = new MoveForwardCommand();
		m_controlMapping[(int)'S'] = new MoveBackwardsCommand();
		m_controlMapping[(int)'A'] = new MoveLeftCommand();
		m_controlMapping[(int)'D'] = new MoveRightCommand();

		m_controlMapping[(int)'R'] = new RotateLeftCommand();
		m_controlMapping[(int)'T'] = new RotateRightCommand();
		m_controlMapping[(int)'F'] = new RotateUpCommand();
		m_controlMapping[(int)'G'] = new RotateDownCommand();

		m_controlMapping[(int)'C'] = new SetFirstPersonCameraCommand();
		m_controlMapping[(int)'V'] = new SetThirdPersonCameraCommand();

	}
	void handleInputs(const std::vector<bool>& keyBuffer) 
	{	
		for (const auto& mapEntry : m_controlMapping)
		{
			if (keyBuffer[mapEntry.first])
			{
				mapEntry.second->execute(*m_object);
			}
		}
	}
};


#endif //INPUTHANDLER_H