// Copyright Terrence Verlander and Isaac Beer

#include "EngineCore.h"
#include <Windows.h>

int main(int argc, char* argv[])
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED); //TEMP TV (might remove later)

	
	I_EngineCore* engineCore;

	engineCore = new EngineCore;
	
	engineCore->InitEngineCore();
	engineCore->RunEngineCore();

	return 0;
}
