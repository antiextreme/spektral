#ifndef GUIMANAGER_H
#define GUIMANAGER_H
#pragma once


#include "ImGui/imgui.h"
#include "ImGui/imgui_impl_glfw.h"
#include "ImGui/imgui_impl_opengl3.h"

class GL_Window;

class GUIManager
{
public:
	static GUIManager& getInstance()
	{
		static GUIManager instance;						
		return instance;
	}

	GUIManager() 
	{
		show_demo_window = false;
		show_another_window = false;
		clear_color = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
	}

	~GUIManager() {};

	int InitImGui(GLFWwindow* window);
	int RenderImGui();
	int DeinitImGui();

private:
	GUIManager(GUIManager const&);              // Don't Implement
	void operator=(GUIManager const&); // Don't implement

	//Imgui Settings
	bool show_demo_window;
	bool show_another_window;
	ImVec4 clear_color;
	const char* glsl_version = "#version 130";
};





inline int GUIManager::InitImGui(GLFWwindow* window)
{
	//Imgui
	// Show the window

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGuiContext* ctx = ImGui::CreateContext();
	ImGui::SetCurrentContext(ctx);
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	ImGui::StyleColorsDark();

	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);

	

	return 0;
}


inline int GUIManager::RenderImGui()
{
	//ImGUI Start
	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	// 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
	if (show_demo_window)
		ImGui::ShowDemoWindow(&show_demo_window);

	// 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
	{
		static float f = 0.0f;
		static int counter = 0;

		//ImGui::SetNextWindowPos(ImVec2(0, 0));
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);

		ImGui::Begin("Spektral Engine");                           // Create a window called "Hello, world!" and append into it.

		
		ImGui::Checkbox("Demo Window", &show_demo_window);       // Edit bools storing our window open/close state
		
		ImGui::SliderFloat("float", &f, 0.0f, 1.0f);             // Edit 1 float using a slider from 0.0f to 1.0f
		ImGui::ColorEdit3("clear color", (float*)& clear_color); // Edit 3 floats representing a color

		if (ImGui::Button("Button"))                             // Buttons return true when clicked (most widgets return true when edited/activated)
			counter++;
		ImGui::SameLine();
		ImGui::Text("counter = %d", counter);

		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);


		ImGui::PopStyleVar();

		ImGui::End();
	}


	// Rendering
	ImGui::EndFrame();

	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());


	return 0;
}

inline int GUIManager::DeinitImGui()
{
	//Destroy imgui
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	return 0;
}



#endif //GUIMANAGER_H