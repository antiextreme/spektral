#ifndef GRAPHICS_ENUMS_H
#define GRAPHICS_ENUMS_H
#pragma once

enum e_windowMode {
	e_windowMode_exclusive_fullscreen,
	e_windowMode_borderless_windowed,
	e_windowMode_windowed
};

//Shader Types
enum e_shader_type
{
	e_shader_type_vertex,
	e_shader_type_fragment,
	e_shader_type_geometry
};



#endif //GRAPHICS_ENUMS_H