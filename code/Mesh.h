#ifndef MESH_H
#define MESH_H

#pragma once

//#include <glad/glad.h> 
#include <assimp/Importer.hpp>
#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
#include <string>
#include <vector>
using namespace std;

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 textureCoords;
};

struct Texture {
	unsigned int id;
	string type;
	aiString filepath;
};

class Mesh
{
public:
	/*  Mesh Data  */
	vector<Vertex> vertices;
	vector<unsigned int> indices;
	vector<Texture> textures;
	unsigned int VAO;

	/*  Functions  */
	Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures);

	// render the mesh with a given shader program
	void render(const unsigned int shaderProgram);

private:
	/*  Render data  */
	unsigned int VBO, EBO;

	/*  Functions    */
	void setupMesh();

};


#endif //MODEL_H