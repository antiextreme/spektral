#pragma once

#include "GameObject.h"

//-------------------------------------------------------------------------------------------
// Name: Entity
// Desc: Dynamic object such as a player or an enemy
//------------------------------------------------------------------------------------------
class Entity : public GameObject
{
public:

	
	virtual void OnUpdate(double dt) override {};
	virtual void OnMessage(const std::string message) override {};


	
private:
	
};

