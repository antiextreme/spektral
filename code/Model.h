#ifndef MODEL_H
#define MODEL_H

#pragma once
#include <string>
#include <vector>
#include "Mesh.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#define STB_IMAGE_IMPLEMENTATION

class Model
{
public:

	Model(std::string filepath);

	void Render(const unsigned int shaderProgram);

	static unsigned int TextureFromFile(const char* filepath, const std::string& directory, bool gamma = false);

private:

	std::vector<Mesh> v_meshes;
	std::string directory;
	std::vector<Texture> v_textures;

	void loadModel(std::string path);
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type,
		std::string typeName);
};


#endif //MODEL_H