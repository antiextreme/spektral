#ifndef KEYBOARD_H
#define KEYBOARD_H

#pragma once

#include "I_Input.h"
#include <vector>

#include <glfw3.h>
#include <glfw3native.h>

//-------------------------------------------------------------------------------------------
// Name: Keyboard
// Desc: Responsible for keyboard input
//-------------------------------------------------------------------------------------------
class Keyboard : I_Input
{
public:

	static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static std::vector<bool> m_key_buffer;
	static const int m_key_bufferSize = 400;

	int Init(EventEngineContainer* events_engine) override;
	int Update() override;

private:



};



#endif //KEYBOARD_H