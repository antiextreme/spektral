#ifndef TRANSFORMCOMPONENT_H
#define TRANSFORMCOMPONENT_H
#pragma once

#include "Component.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>

#include <iostream>


//-------------------------------------------------------------------------------------------
// Name: TransformComponent
// Desc: Responsible for transformations
//------------------------------------------------------------------------------------------
class TransformComponent : public Component
{
private:
	// values for HP home desktop
	const float moveStepsize = 0.004f; // 0.004f;  accurate binary value
	const float rotateStepsize = 1.0f / 512.0f;	// 0.001f;

private:
	glm::quat m_orientationYaw;
	glm::quat m_orientationPitch;
	glm::vec3 m_position;
	glm::vec3 m_scale;

public:

	void OnUpdate(double dt) override;

	void OnMessage(const std::string m) override;

	TransformComponent() : m_position(0), m_orientationYaw(1, 0, 0, 0), m_orientationPitch(1, 0, 0, 0), m_scale(1.0f) {}
	TransformComponent(const glm::vec3& pos) : m_position(pos), m_orientationYaw(1, 0, 0, 0), m_orientationPitch(1, 0, 0, 0), m_scale(1.0f) {}

	TransformComponent(const glm::vec3& pos, const glm::quat& orientY) : m_position(pos), m_orientationYaw(orientY), m_orientationPitch(1, 0, 0, 0), m_scale(1.0f) {}
	TransformComponent(const glm::vec3& pos, const glm::quat& orientY, const glm::quat& orientP) : m_position(pos), m_orientationYaw(orientY), m_orientationPitch(orientP), m_scale(1.0f) {}

	const glm::vec3& GetPosition() const { return m_position; }
	const glm::vec3& GetScale() const { return m_scale; }

	glm::quat GetOrientation() { return m_orientationYaw * m_orientationPitch; }

	glm::mat4 getModelMatrix()
	{
		glm::mat4 transMatrix = glm::translate(glm::mat4(1.0f), m_position);
		glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), m_scale);
		glm::mat4 rotMatrix = glm::mat4_cast(GetOrientation());
		return transMatrix * rotMatrix * scaleMatrix;
	}

	void translate(const glm::vec3& v) { m_position += v; }
	void translate(float x, float y, float z) { m_position += glm::vec3(x, y, z); }

	void scaleUp(const glm::vec3& v) { m_scale += v; }
	void scaleUp(float x, float y, float z) { m_scale += glm::vec3(x, y, z); }

	void yaw(float angle){ m_orientationYaw *= glm::angleAxis(angle, glm::vec3(0, 1, 0)); }
	void pitch(float angle){ m_orientationPitch *= glm::angleAxis(angle, glm::vec3(1, 0, 0)); }

	glm::vec3 GetEulerAngles(){glm::quat m_orientation = m_orientationYaw * m_orientationPitch;return glm::eulerAngles(m_orientation);  }
};


#endif //TRANSFORMCOMPONENT_H